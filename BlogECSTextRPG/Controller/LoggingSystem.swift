//
//  LoggingSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/8/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class LoggingSystem {
    
    // MARK: - Singleton
    static let instance = LoggingSystem ()
    fileprivate init() {}
    
    // MARK: - Fields
    fileprivate var logs: [String] = []
    fileprivate var maxLogCount = 100
    
    // MARK: - Public Methods
    func addLog (_ message: String) {
        logs.append(message)
        if (logs.count > maxLogCount) {
            logs.removeFirst()
        }
    }
    
    func print () -> String {
        return logs.joined(separator: "\n")
    }
}
