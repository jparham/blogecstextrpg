//
//  InterpreterSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/16/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class InterpreterSystem {
    
    // MARK: - Singleton
    static let instance = InterpreterSystem()
    
    // MARK: - Fields
    fileprivate var verbs: Set<String> = []
    fileprivate lazy var verbRegex: String = {
        return "\\b\(self.verbs.joined(separator: "\\b|\\b"))\\b"
    }()
    
    fileprivate var prepositions: Set<String> = []
    fileprivate lazy var prepositionRegex: String = {
        return "\\b\(self.prepositions.joined(separator: "\\b|\\b"))\\b"
    }()
    
    fileprivate var handlers: [UserAction] = []
    
    // MARK: - Public
    func register(_ action: UserAction) {
        verbs.formUnion(action.commands)
        prepositions.formUnion(action.specifiers)
        handlers.append(action)
    }
    
    func handleUserInput(_ input: String) {
        let result = interpret(input)
        
        guard result !== Interpretation.invalid else {
            LoggingSystem.instance.addLog("I don't understand")
            return
        }
        
        for handler in handlers {
            handler.handle(result)
        }
    }
    
    // MARK: - Private
    fileprivate func interpret(_ input: String) -> Interpretation {
        var text = input.trimmingCharacters(in: CharacterSet.whitespaces)
        
        var command: String = ""
        var specifier: String = ""
        var primary: [Target] = []
        var secondary: [Target] = []
        
        let searchOptions: NSString.CompareOptions = [NSString.CompareOptions.regularExpression, NSString.CompareOptions.caseInsensitive]
        guard let commandRange = text.range(of: verbRegex, options: searchOptions) else {
            return Interpretation.invalid
        }
        
        command = text[commandRange].uppercased()
        
        text.removeSubrange(commandRange)
        text = text.trimmingCharacters(in: CharacterSet.whitespaces)
        
        if let specifierRange = text.range(of: prepositionRegex, options: searchOptions) {
            specifier = text[specifierRange].uppercased()
            
            var elements: [String] = []
            
            let preText = text[text.startIndex ..< specifierRange.lowerBound]
            if preText.count > 0 {
                elements.append(String(preText))
            }
            
            let postText = text[specifierRange.upperBound ..< text.endIndex]
            if postText.count > 0 {
                elements.append(String(postText))
            }
            
            if let preTargets = elements.first {
                primary = splitTargets(preTargets)
            }
            
            if let postTargets = elements.last, elements.first != elements.last {
                secondary = splitTargets(postTargets)
            }
        } else {
            primary = splitTargets(text)
        }
        
        return Interpretation(command: command, specifier: specifier, primary: primary, secondary: secondary)
    }
    
    fileprivate func splitTargets(_ text: String) -> [Target] {
        guard let regex = try? NSRegularExpression(pattern: "\\b(?i)AND\\b", options: []) else { return [] }
        let nsString = text as NSString
        let splitter = ","
        let modifiedString = regex.stringByReplacingMatches(in: text, options: [], range: NSRange(location: 0, length: nsString.length), withTemplate: splitter)
        let entries = modifiedString.components(separatedBy: splitter).map({ $0.trimmingCharacters(in: CharacterSet.whitespaces) }).filter({ $0 != "" })
        var items: [Target] = []
        for entry in entries {
            let item = mapTarget(entry)
            items.append(item)
        }
        return items
    }
    
    fileprivate func mapTarget(_ input: String) -> Target {
        var words = input.components(separatedBy: CharacterSet.whitespaces).filter({ $0.count > 0 })
        
        var noun: Noun?
        for (index, element) in words.enumerated() {
            if let result = LabelSystem.fetchNoun(element) {
                noun = result
                words.remove(at: index)
                break
            }
        }
        
        guard let matchingNoun = noun else { return Target(userInput: input, candidates: []) }
        let entities = LabelSystem.fetchEntitiesForNoun(matchingNoun)
        
        var adjectiveIDs: [Int64] = []
        for word in words {
            if let adjective = LabelSystem.fetchAdjective(word) {
                adjectiveIDs.append(adjective.id)
            }
        }
        
        let candidates = entities.filter { (entity) -> Bool in
            let entityAdjectiveIDs = entity.getAdjectives().map({ $0.id })
            for adjectiveID in adjectiveIDs {
                if !entityAdjectiveIDs.contains(adjectiveID) {
                    return false
                }
            }
            return true
        }
        
        return Target(userInput: input, candidates: candidates)
    }
}
