//
//  OpenableSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

class OpenableSystem {
    
    class func setup() {
        InterpreterSystem.instance.register(OpenAction())
        InterpreterSystem.instance.register(CloseAction())
    }
    
    class func open(_ item: Openable) -> String {
        guard item.isOpen == false else {
            return "It is already open."
        }
        
        guard item.isLocked == false else {
            return item.openMessage ?? "You can't open it."
        }
        
        setOpen(item, value: true)
        TriggerSystem.execute(item.openTrigger)
        return item.openMessage ?? (SightSystem.describeOpening(item) ?? "Opened.")
    }
    
    class func close(_ item: Openable) -> String {
        guard item.isOpen == true else {
            return "It is already closed."
        }
        
        guard item.isLocked == false else {
            return item.closeMessage ?? "You can't close it."
        }
        
        setOpen(item, value: false)
        TriggerSystem.execute(item.closeTrigger)
        return item.closeMessage ?? "Closed."
    }
    
    fileprivate class func setOpen(_ item: Openable, value: Bool) {
        let update = Openable.table.filter(Openable.openable_id_column == item.id).update(Openable.is_open_column <- value)
        DataManager.instance.run(update)
    }
    
    fileprivate class func setLock(_ item: Openable, value: Bool) {
        let update = Openable.table.filter(Openable.openable_id_column == item.id).update(Openable.is_locked_column <- value)
        DataManager.instance.run(update)
    }
}
