//
//  SightSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

class SightSystem {
    
    // MARK: - Public
    class func setup() {
        InterpreterSystem.instance.register(LookAction())
    }
    
    class func describeOpening(_ openable: Openable) -> String? {
        guard let entity = openable.entity else { return .none }
        let containerName = LabelSystem.describe(entity)
        let items = SightSystem.listContainerContents(entity)
        guard items.count > 0 else { return .none }
        let message = items.count == 1 ? "Opening the \(containerName) reveals a \(items[0])." : "Opening the \(containerName) reveals:\n  \(items.joined(separator: "\n  "))"
        return message
    }
    
    class func listContainerContents(_ entity: Entity) -> [String] {
        guard let _ = entity.getContainer() else { return [] }
        if let openable = entity.getOpenable() {
            guard openable.isOpen else { return [] }
        }
        let contents = ContainmentSystem.fetchContainedEntities(entity)
        var items: [String] = []
        for item in contents {
            let name = LabelSystem.describe(item)
            items.append(name)
        }
        return items
    }
    
    class func describeContainerContents(_ entity: Entity) -> String? {
        let containerName = LabelSystem.describe(entity)
        let items = SightSystem.listContainerContents(entity)
        guard items.count > 0 else { return .none }
        return "The \(containerName) contains:\n  \(items.joined(separator: "\n  "))"
    }
    
    class func describeRoom(_ room: Room, verbose: Bool) {
        var message = room.visited == false || verbose ? "\(room.title)\n\(room.description)" : room.title
        message = appendDescription(message, ofContentsOfRoom: room)
        LoggingSystem.instance.addLog(message)
    }
    
    class func examine(_ entity: Entity, userDescription: String?) -> String {
        let label = userDescription ?? LabelSystem.describe(entity)
        if let openable = entity.getOpenable() {
            if openable.isOpen == false {
                return "The \(label) is closed."
            } else if entity.getContainer() == nil {
                return "The \(label) is open."
            }
        }
        
        if let _ = entity.getContainer(), let contents = describeContainerContents(entity) {
            return contents
        }
        
        return "There's nothing special about the \(label)."
    }
    
    // MARK: - Private
    fileprivate class func appendDescription(_ description: String, ofContentsOfRoom room: Room) -> String {
        var updatedDescription = description
        let entities = RoomSystem.entitiesInRoom()
        for entity in entities {
            guard entity.id != PlayerSystem.player.id else { continue }
            if let _ = entity.getNotable() {
                updatedDescription += appendDescription(entity)
            }
            if let _ = entity.getContainer(), let contents = describeContainerContents(entity) {
                updatedDescription += "\n\(contents)"
            }
        }
        return updatedDescription
    }
    
    fileprivate class func appendDescription(_ entity: Entity) -> String {
        let itemDescription = LabelSystem.describe(entity)
        return "\nThere is a \(itemDescription) here."
    }
}
