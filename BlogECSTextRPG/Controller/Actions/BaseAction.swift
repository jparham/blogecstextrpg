//
//  BaseAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

enum TargetMode {
    case any        // Matches any case
    case zero       // Expects exactly zero targets
    case upToOne    // Expects zero or one target
    case single     // Expects exactly one target
    case oneOrMore  // Expects one or more targets
    case multiple   // Expects more than one target
}

class BaseAction: UserAction {
    static let invalidSentence = "That sentence isn't one I recognize."
    let commands: Set<String>
    let specifiers: Set<String>
    let primaryTargetMode: TargetMode
    let secondaryTargetMode: TargetMode
    
    init(commands: Set<String>, specifiers: Set<String>, primaryTargetMode: TargetMode, secondaryTargetMode: TargetMode) {
        self.commands = commands
        self.specifiers = specifiers
        self.primaryTargetMode = primaryTargetMode
        self.secondaryTargetMode = secondaryTargetMode
    }
    
    func canHandle(_ interpretation: Interpretation) -> Bool {
        guard commands.contains(interpretation.command) else { return false }
        if specifiers.count > 0 && !specifiers.contains(interpretation.specifier) { return false }
        if interpretation.specifier.count > 0 && !specifiers.contains(interpretation.specifier) { return false }
        guard checkMatch(interpretation.primary, mode: primaryTargetMode) else { return false }
        guard checkMatch(interpretation.secondary, mode: secondaryTargetMode) else { return false }
        return true
    }
    
    fileprivate func checkMatch(_ targets: [Target], mode: TargetMode) -> Bool {
        switch mode {
        case .any:
            return true
        case .zero:
            return targets.count == 0
        case .upToOne:
            return targets.count <= 1
        case .single:
            return targets.count == 1
        case .oneOrMore:
            return targets.count >= 1
        case .multiple:
            return targets.count > 1
        }
    }
    
    func handle(_ interpretation: Interpretation) {
        
    }
}
