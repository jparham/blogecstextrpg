//
//  EatAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class EatAction: CompoundAction {
    init() {
        let noTarget = NoTargetErrorAction(commands: ["EAT"])
        let target = EatTargetAction(commands: ["EAT"], specifiers: [], primaryTargetMode: .single, secondaryTargetMode: .zero)
        super.init(actions: [noTarget, target])
    }
}

private class EatTargetAction: BaseAction {
    fileprivate func customEatFilter(_ target: Target) {
        target.candidates = target.candidates.filter({ (entity) -> Bool in
            return entity.getEatable() != nil
        })
        if target.candidates.count == 0 {
            target.error = "I don't think that the \(target.userInput) would agree with you."
            return
        }
    }
    
    override func handle(_ interpretation: Interpretation) {
        guard let target = interpretation.primary.first else { return }
        
        TargetingSystem.filter(target, options: [TargetingFilter.CurrentRoom, TargetingFilter.ContainerIsOpen])
        customEatFilter(target)
        TargetingSystem.filter(target, options: TargetingFilter.HeldByPlayer)
        TargetingSystem.validate(target)
        
        guard let match = target.match, let eatable = match.getEatable(), target.error == .none else {
            guard let error = target.error else { return }
            LoggingSystem.instance.addLog(error)
            return
        }
        
        let message = EatableSystem.eat(eatable)
        LoggingSystem.instance.addLog(message)
    }
}
