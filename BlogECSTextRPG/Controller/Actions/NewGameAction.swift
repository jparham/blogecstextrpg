//
//  NewGameAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class NewGameAction: CompoundAction {
    init() {
        let action = BaseNewGameAction(commands: ["NEW"], specifiers: [], primaryTargetMode: .zero, secondaryTargetMode: .zero)
        super.init(actions: [action])
    }
}

private class BaseNewGameAction: BaseAction {
    override fileprivate func handle(_ interpretation: Interpretation) {
        LoggingSystem.instance.addLog("Beginning new game...")
        GameSystem.newGame()
    }
}
