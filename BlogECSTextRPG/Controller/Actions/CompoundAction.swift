//
//  CompoundAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class CompoundAction: BaseAction {
    let actions: [BaseAction]
    let fallback: ((Interpretation) -> ())?
    
    init (actions: [BaseAction], fallback:((Interpretation) -> ())? = .none) {
        self.actions = actions
        self.fallback = fallback
        
        var allCommands: Set<String> = []
        var allSpecifiers: Set<String> = []
        for action in actions {
            allCommands.formUnion(action.commands)
            allSpecifiers.formUnion(action.specifiers)
        }
        
        super.init(commands: allCommands, specifiers: allSpecifiers, primaryTargetMode: .any, secondaryTargetMode: .any)
    }
    
    override func canHandle(_ interpretation: Interpretation) -> Bool {
        for action in actions {
            if action.canHandle(interpretation) {
                return true
            }
        }
        return false
    }
    
    override func handle(_ interpretation: Interpretation) {
        for action in actions {
            if action.canHandle(interpretation) {
                action.handle(interpretation)
                return
            }
        }
        defaultFallback(interpretation)
    }
    
    func defaultFallback(_ interpretation: Interpretation) {
        guard let customFallback = fallback else {
            if commands.contains(interpretation.command) {
                LoggingSystem.instance.addLog(BaseAction.invalidSentence)
            }
            return
        }
        customFallback(interpretation)
    }
}
