//
//  ReadAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class ReadAction: CompoundAction {
    init() {
        let noTarget = NoTargetErrorAction(commands: ["READ"])
        let target = ReadTargetAction(commands: ["READ"], specifiers: [], primaryTargetMode: .single, secondaryTargetMode: .zero)
        super.init(actions: [noTarget, target])
    }
}

class ReadTargetAction: BaseAction {
    fileprivate func customReadFilter(_ target: Target) {
        target.candidates = target.candidates.filter({ (entity) -> Bool in
            return entity.getReadable() != nil
        })
        if target.candidates.count == 0 {
            target.error = "How does one read a \(target.userInput)?"
            return
        }
    }
    
    override func handle(_ interpretation: Interpretation) {
        guard let target = interpretation.primary.first else { return }
        
        TargetingSystem.filter(target, options: [TargetingFilter.CurrentRoom, TargetingFilter.ContainerIsOpen])
        customReadFilter(target)
        TargetingSystem.filter(target, options: [TargetingFilter.HeldByPlayer])
        TargetingSystem.validate(target)
        
        guard let match = target.match, let readable = match.getReadable(), target.error == .none else {
            guard let error = target.error else { return }
            LoggingSystem.instance.addLog(error)
            return
        }
        
        let message = ReadableSystem.read(readable)
        LoggingSystem.instance.addLog(message)
    }
}
