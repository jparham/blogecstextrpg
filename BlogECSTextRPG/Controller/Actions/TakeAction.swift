//
//  TakeAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class TakeAction: CompoundAction {
    init() {
        let noTarget = NoTargetErrorAction(commands: ["TAKE"])
        let target = TakeTargetAction(commands: ["TAKE"], specifiers: [], primaryTargetMode: .oneOrMore, secondaryTargetMode: .zero)
        let targetFrom = TakeTargetFromAction(commands: ["TAKE"], specifiers: ["FROM"], primaryTargetMode: .oneOrMore, secondaryTargetMode: .single)
        super.init(actions: [noTarget, target, targetFrom])
    }
}

private class BaseTakeAction: BaseAction {
    static let takeErrors = ["What a concept!", "An interesting idea...", "You can't be serious.", "A valiant attempt."]
    
    func randomTakeError() -> String {
        let randomIndex = Int(arc4random_uniform(UInt32(BaseTakeAction.takeErrors.count)))
        return BaseTakeAction.takeErrors[randomIndex]
    }
    
    func customTakeFiltering(_ target: Target) {
        target.candidates = target.candidates.filter({ $0.getTakeable() != nil })
        if target.candidates.count == 0 {
            target.error = randomTakeError()
            return
        }
    }
    
    func takeTarget(_ target: Target, containerTarget: Target?) -> String {
        TargetingSystem.filter(target, options: [TargetingFilter.CurrentRoom, TargetingFilter.ContainerIsOpen, TargetingFilter.NotHeldByPlayer])
        customTakeFiltering(target)
        TargetingSystem.validate(target)
        
        guard let match = target.match, let takeable = match.getTakeable(), target.error == nil else {
            return target.error ?? "\(String(describing: containerTarget)): You can't do that."
        }
        
        if let containerTarget = containerTarget {
            guard ContainmentSystem.checkContainment(match, containingEntity: containerTarget.match) else {
                return "The \(target.userInput) isn't in the \(containerTarget.userInput)"
            }
        }
        
        let message = TakeableSystem.take(takeable)
        return "\(target.userInput): \(message)"
    }
}

private class TakeTargetAction: BaseTakeAction {
    override func handle(_ interpretation: Interpretation) {
        var message = ""
        for (index, target) in interpretation.primary.enumerated() {
            if index > 0 {
                message += "\n"
            }
            message += takeTarget(target, containerTarget: .none)
        }
        
        LoggingSystem.instance.addLog(message)
    }
}

private class TakeTargetFromAction: BaseTakeAction {
    override func handle(_ interpretation: Interpretation) {
        guard let containerTarget = interpretation.secondary.first else { return }
        TargetingSystem.filter(containerTarget, options: [TargetingFilter.CurrentRoom, TargetingFilter.ContainerIsOpen])
        TargetingSystem.validate(containerTarget)
        
        guard let _ = containerTarget.match, containerTarget.error == nil else {
            guard let error = containerTarget.error else { return }
            LoggingSystem.instance.addLog(error)
            return
        }
        
        var message = ""
        for (index, target) in interpretation.primary.enumerated() {
            if index > 0 {
                message += "\n"
            }
            message += takeTarget(target, containerTarget: containerTarget)
        }
        
        LoggingSystem.instance.addLog(message)
    }
}
