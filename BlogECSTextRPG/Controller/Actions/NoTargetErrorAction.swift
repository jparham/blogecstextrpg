//
//  NoTargetErrorAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class NoTargetErrorAction: BaseAction {
    init(commands: Set<String>) {
        super.init(commands: commands, specifiers: [], primaryTargetMode: .zero, secondaryTargetMode: .zero)
    }
    
    override func handle(_ interpretation: Interpretation) {
        let message = "What do you want to \(interpretation.command.lowercased())?"
        LoggingSystem.instance.addLog(message)
    }
}
