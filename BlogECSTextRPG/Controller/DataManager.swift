//
//  DataManager.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/12/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

class DataManager {
    
    // MARK: - Fields
    fileprivate var database: Connection!
    
    // A path to the default database included in the app bundle
    fileprivate var defaultDataPath: String {
        get {
            guard let retValue = Bundle.main.path(forResource: "Zork", ofType: "db") else {
                fatalError("Invalid path to default database File")
            }
            return retValue
        }
    }
    
    // A path to a saved game in progress
    fileprivate var userDataPath: String {
        get {
            guard let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else {
                fatalError("Cannot access documents directory")
            }
            let retValue = NSString(string: documentsPath).appendingPathComponent("Zork.db")
            return retValue
        }
    }
    
    // A path to an un-saved game in progress
    fileprivate var tempDataPath: String {
        get {
            let retValue = NSString(string: NSTemporaryDirectory()).appendingPathComponent("Zork.db")
            return retValue
        }
    }
    
    // MARK: - Singleton
    static let instance = DataManager()
    fileprivate init() {}
    
    // MARK: - Public
    func prepare(_ table: Table) -> [Row] {
        do {
            let result = try database.prepare(table)
            return Array(result)
        } catch {
            
        }
        return []
    }
    
    func run(_ update: Update) {
        do {
            try database.run(update)
        } catch {
            
        }
    }
    
    func save() {
        do {
            let userDataURL = URL(fileURLWithPath: userDataPath), tempDataURL = URL(fileURLWithPath: tempDataPath)
            _ = try FileManager.default.replaceItemAt(userDataURL, withItemAt: tempDataURL)
            load()
        } catch {
            
        }
    }
    
    func load() {
        // If the user has a game in progress, start with that, otherwise begin from the default database
        let path = FileManager.default.fileExists(atPath: userDataPath) ? userDataPath : defaultDataPath
        
        // Create a copy of the database from the bundle to the users documents folder if necessary
        do {
            if FileManager.default.fileExists(atPath: tempDataPath) {
                try FileManager.default.removeItem(atPath: tempDataPath)
            }
            try FileManager.default.copyItem(atPath: path, toPath: tempDataPath)
        } catch {
            fatalError("Unable to copy database")
        }
        
        // Connect to the copied database so we are not restricted by read-only mode
        do {
            database = try Connection(tempDataPath)
        } catch {
            fatalError("Unable to connect to database")
        }
    }
    
    func reset() {
        do {
            if FileManager.default.fileExists(atPath: userDataPath) {
                try FileManager.default.removeItem(atPath: userDataPath)
            }
        } catch {
            
        }
        load()
    }
}
