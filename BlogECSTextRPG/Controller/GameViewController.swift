//
//  GameViewController.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/8/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet fileprivate var historyTextView: UITextView!
    @IBOutlet fileprivate var commandTextField: UITextField!
    @IBOutlet fileprivate var stackBottomConstraint: NSLayoutConstraint!
    
    // MARK: - Fields
    fileprivate let bottomPadding: CGFloat = 16
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        MasterSystem.setup()
        historyTextView.text = LoggingSystem.instance.print()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow (_:)), name: NSNotification.Name.UIKeyboardWillShow, object: .none)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide (_:)), name: NSNotification.Name.UIKeyboardWillHide, object: .none)
        commandTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: .none)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: .none)
    }
    
    // MARK: - Notification Handlers
    @objc func keyboardWillShow (_ notification: Notification) {
        guard let userInfo = notification.userInfo, let keyboardSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        stackBottomConstraint.constant = keyboardSize.height + bottomPadding
    }
    
    @objc func keyboardWillHide (_ notification: Notification) {
        stackBottomConstraint.constant = bottomPadding
    }
    
    // MARK: - Private
    fileprivate func scrollToCurrent() {
        let time = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time) { [weak self] in
            guard let textView = self?.historyTextView else { return }
            let offset = CGPoint(x: 0, y: max(textView.contentSize.height - textView.bounds.height, 0))
            textView.setContentOffset(offset, animated: true)
        }
    }
}

extension GameViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let input = textField.text, let message = String(validatingUTF8: "\n> \(input)"), input.count > 0 else { return true }
        LoggingSystem.instance.addLog(message)
        InterpreterSystem.instance.handleUserInput(input)
        textField.text = "";
        historyTextView.text = LoggingSystem.instance.print()
        scrollToCurrent()
        return true
    }
}
