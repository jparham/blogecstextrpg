//
//  PlayerSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/19/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class PlayerSystem {
    static var player: Entity {
        get {
            guard let entity = Entity.fetchByID(1) else { fatalError("Can't locate the Player") }
            return entity
        }
    }
    
    class func setup() {
        InterpreterSystem.instance.register(InventoryAction())
    }
    
    class func heldByPlayer(_ containable: Containable?) -> Bool {
        guard let containable = containable else { return false }
        guard let rootEntity = ContainmentSystem.rootEntity(containable) else { return false }
        return rootEntity.id == player.id
    }
}
