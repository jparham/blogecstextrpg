//
//  EatableSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class EatableSystem {
    class func setup() {
        InterpreterSystem.instance.register(EatAction())
    }
    
    class func eat(_ eatable: Eatable) -> String {
        if let containable = eatable.entity?.getContainable() {
            ContainmentSystem.move(containable, containingEntityID: 0)
        }
        
        let message = eatable.message ?? "Eaten."
        return message
    }
}
