//
//  GameSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class GameSystem {
    class func setup() {
        InterpreterSystem.instance.register(SaveGameAction())
        InterpreterSystem.instance.register(LoadGameAction())
        InterpreterSystem.instance.register(NewGameAction())
        load()
    }
    
    class func load() {
        DataManager.instance.load()
        RoomSystem.move(RoomSystem.room.id)
    }
    
    class func save() {
        DataManager.instance.save()
    }
    
    class func newGame() {
        DataManager.instance.reset()
        RoomSystem.move(RoomSystem.room.id)
    }
}
