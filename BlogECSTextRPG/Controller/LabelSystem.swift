//
//  LabelSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/16/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

class LabelSystem {
    class func fetchNoun(_ text: String) -> Noun? {
        let textNoCase = Expression<String>("text").collate(.nocase)
        let table = Noun.table.filter(textNoCase == text)
        guard let match = DataManager.instance.prepare(table).first else { return .none }
        let result = Noun(row: match)
        return result
    }
    
    class func fetchAdjective(_ text: String) -> Adjective? {
        let textNoCase = Expression<String>("text").collate(.nocase)
        let table = Adjective.table.filter(textNoCase == text)
        guard let match = DataManager.instance.prepare(table).first else { return .none }
        let result = Adjective(row: match)
        return result
    }
    
    class func fetchEntitiesForNoun(_ noun: Noun) -> [Entity] {
        let result = Entity.fetchAllByComponentID(Noun.component_id, dataID: noun.id)
        return result
    }
    
    class func describe(_ entity: Entity) -> String {
        if let description = entity.getNotable()?.description {
            return description
        }
        if let noun = entity.getNouns().first {
            let adjectives = entity.getAdjectives().map({ $0.text }).joined(separator: " ")
            return adjectives.count > 0 ? "\(adjectives) \(noun.text)" : noun.text
        } else {
            fatalError("Cannot describe an entity without a notable or noun component")
        }
    }
}
