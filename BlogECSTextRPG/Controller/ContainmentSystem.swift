//
//  ContainmentSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

class ContainmentSystem {
    class func rootEntity(_ containable: Containable) -> Entity? {
        guard let containingEntity = containable.containingEntity else { return containable.entity }
        guard let parentContainable = containingEntity.getContainable() else { return containingEntity }
        return rootEntity(parentContainable)
    }
    
    class func fetchContainedEntities(_ entity: Entity) -> [Entity] {
        let table = Containable.table.filter(Containable.containing_entity_id_column == entity.id)
        let containables = DataManager.instance.prepare(table).map({ Containable(row: $0) })
        var result: [Entity] = []
        for item in containables {
            guard let entity = item.entity else { continue }
            result.append(entity)
        }
        return result
    }
    
    class func canContain(_ containable: Containable, container: Container) -> Bool {
        let occupiedSpace = ContainmentSystem.occupiedSpace(container)
        let hasRoom = container.capacity - occupiedSpace >= containable.size
        return hasRoom
    }
    
    class func occupiedSpace(_ container: Container) -> Double {
        var usedSpace: Double = 0
        guard let containerEntity = container.entity else { return usedSpace }
        let contents = ContainmentSystem.fetchContainedEntities(containerEntity)
        for entity in contents {
            guard let containable = entity.getContainable() else { continue }
            usedSpace += containable.size
        }
        return usedSpace
    }
    
    class func move(_ containable: Containable, containingEntityID: Int64) {
        let update = Containable.table.filter(Containable.containable_id_column == containable.id).update(Containable.containing_entity_id_column <- containingEntityID)
        DataManager.instance.run(update)
    }
    
    class func checkContainment(_ entity: Entity, containingEntity: Entity?) -> Bool {
        guard let containable = entity.getContainable() else { return false }
        guard let containingEntity = containingEntity else { return false }
        guard containable.containingEntityID == containingEntity.id else { return false }
        return true
    }
}
