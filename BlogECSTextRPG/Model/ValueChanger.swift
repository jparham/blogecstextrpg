//
//  ValueChanger.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct ValueChanger {
    
    // MARK: - Fields
    static let component_id: Int64 = 9
    static let table = Table("ValueChangers")
    static let value_changer_id_column = Expression<Int64>("id")
    static let component_id_column = Expression<Int64>("component_id")
    static let component_data_id_column = Expression<Int64>("component_data_id")
    static let column_name_column = Expression<String>("column_name")
    static let change_int_column = Expression<Int64?>("change_int")
    static let change_text_column = Expression<String?>("change_text")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[ValueChanger.value_changer_id_column]
        }
    }
    
    var componentID: Int64 {
        get {
            return row[ValueChanger.component_id_column]
        }
    }
    
    var componentDataID: Int64 {
        get {
            return row[ValueChanger.component_data_id_column]
        }
    }
    
    var columnName: String {
        get {
            return row[ValueChanger.column_name_column]
        }
    }
    
    var changeInt: Int64? {
        get {
            return row[ValueChanger.change_int_column]
        }
    }
    
    var changeText: String? {
        get {
            return row[ValueChanger.change_text_column]
        }
    }
    
    var entity: Entity? {
        get {
            return Entity.fetchByComponentID(ValueChanger.component_id, dataID: id)
        }
    }
}

extension Entity {
    func getValueChangers() -> [ValueChanger] {
        let components = getComponents(ValueChanger.component_id)
        let ids = components.map({ $0.dataID })
        let table = ValueChanger.table.filter(ids.contains(ValueChanger.value_changer_id_column))
        let result = DataManager.instance.prepare(table).map({ ValueChanger(row: $0) })
        return result
    }
}
