//
//  Target.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/16/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class Target {
    let userInput: String
    var candidates: [Entity]
    var match: Entity? = .none
    var error: String? {
        get {
            return _error
        }
        set(newValue) {
            _error = _error ?? newValue
        }
    }
    fileprivate var _error: String? = .none
    
    init(userInput: String, candidates: [Entity]) {
        self.userInput = userInput
        self.candidates = candidates
    }
}
