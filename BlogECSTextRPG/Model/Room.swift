//
//  Room.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/19/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Room {
    
    // MARK: - Fields
    static let component_id: Int64 = 1
    static let table = Table("Rooms")
    static let room_id_column = Expression<Int64>("id")
    static let title_column = Expression<String>("title")
    static let description_column = Expression<String>("description")
    static let visited_column = Expression<Bool>("visited")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Room.room_id_column]
        }
    }
    
    var title: String {
        get {
            return row[Room.title_column]
        }
    }
    
    var description: String {
        get {
            return row[Room.description_column]
        }
    }
    
    var visited: Bool {
        get {
            return row[Room.visited_column]
        }
    }
    
    var entity: Entity? {
        get {
            return Entity.fetchByComponentID(Room.component_id, dataID: id)
        }
    }
}

extension Entity {
    func getRoom() -> Room? {
        guard let component = getComponent(Room.component_id) else { return .none }
        let table = Room.table.filter(Room.room_id_column == component.dataID)
        guard let result = DataManager.instance.prepare(table).first else { return .none }
        return Room(row: result)
    }
}
