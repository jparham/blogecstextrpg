//
//  Adjective.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/16/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Adjective {
    
    // MARK: - Fields
    static let component_id: Int64 = 11
    static let table = Table("Adjectives")
    static let adjective_id_column = Expression<Int64>("id")
    static let text_column = Expression<String>("text")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Adjective.adjective_id_column]
        }
    }
    
    var text: String {
        get {
            return row[Adjective.text_column]
        }
    }
}

extension Entity {
    func getAdjectives() -> [Adjective] {
        let components = getComponents(Adjective.component_id)
        let ids = components.map({ $0.dataID })
        let table = Adjective.table.filter(ids.contains(Adjective.adjective_id_column))
        let result = DataManager.instance.prepare(table).map({ Adjective(row: $0) })
        return result
    }
}