//
//  Drinkable.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Drinkable {
    
    // MARK: - Fields
    static let component_id: Int64 = 13
    static let table = Table("Drinkables")
    static let drinkable_id_column = Expression<Int64>("id")
    static let message_column = Expression<String?>("message")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Drinkable.drinkable_id_column]
        }
    }
    
    var message: String? {
        get {
            return row[Drinkable.message_column]
        }
    }
    
    var entity: Entity? {
        get {
            return Entity.fetchByComponentID(Drinkable.component_id, dataID: id)
        }
    }
}

extension Entity {
    func getDrinkable() -> Drinkable? {
        guard let component = getComponent(Drinkable.component_id) else { return .none }
        let table = Drinkable.table.filter(Drinkable.drinkable_id_column == component.dataID)
        guard let result = DataManager.instance.prepare(table).first else { return .none }
        return Drinkable(row: result)
    }
}
