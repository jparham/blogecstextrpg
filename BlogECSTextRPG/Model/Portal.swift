//
//  Portal.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Portal {
    
    // MARK: - Fields
    static let component_id: Int64 = 2
    static let table = Table("Portals")
    static let portal_id_column = Expression<Int64>("id")
    static let from_room_id_column = Expression<Int64>("from_room_id")
    static let to_room_id_column = Expression<Int64>("to_room_id")
    static let direction_column = Expression<String>("direction")
    static let message_column = Expression<String?>("message")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Portal.portal_id_column]
        }
    }
    
    var fromRoomID: Int64 {
        get {
            return row[Portal.from_room_id_column]
        }
    }
    
    var toRoomID: Int64 {
        get {
            return row[Portal.to_room_id_column]
        }
    }
    
    var direction: String {
        get {
            return row[Portal.direction_column]
        }
    }
    
    var message: String? {
        get {
            return row[Portal.message_column]
        }
    }
    
    var entity: Entity? {
        get {
            return Entity.fetchByComponentID(Portal.component_id, dataID: id)
        }
    }
}

extension Entity {
    func getPortals() -> [Portal] {
        let components = getComponents(Portal.component_id)
        let ids = components.map({ $0.dataID })
        let table = Portal.table.filter(ids.contains(Portal.portal_id_column))
        let result = DataManager.instance.prepare(table).map({ Portal(row: $0) })
        return result
    }
}
