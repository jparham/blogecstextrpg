//
//  Presence.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/19/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Presence {
    
    // MARK: - Fields
    static let component_id: Int64 = 14
    static let table = Table("Presences")
    static let presence_id_column = Expression<Int64>("id")
    static let room_id_column = Expression<Int64>("room_id")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Presence.presence_id_column]
        }
    }
    
    var roomID: Int64 {
        get {
            return row[Presence.room_id_column]
        }
    }
    
    var entity: Entity? {
        get {
            return Entity.fetchByComponentID(Presence.component_id, dataID: id)
        }
    }
}

extension Entity {
    func getPresences() -> [Presence] {
        let components = getComponents(Presence.component_id)
        let ids = components.map({ $0.dataID })
        let table = Presence.table.filter(ids.contains(Presence.presence_id_column))
        let result = DataManager.instance.prepare(table).map({ Presence(row: $0) })
        return result
    }
}