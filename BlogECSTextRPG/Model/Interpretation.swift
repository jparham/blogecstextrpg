//
//  Interpretation.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/16/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class Interpretation {
    static let invalid = Interpretation(command: "", specifier: "", primary: [], secondary: [])
    
    let command: String
    let specifier: String
    let primary: [Target]
    let secondary: [Target]
    
    init(command: String, specifier: String, primary: [Target], secondary: [Target]) {
        self.command = command
        self.specifier = specifier
        self.primary = primary
        self.secondary = secondary
    }
}
