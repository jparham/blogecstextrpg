//
//  Takeable.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Takeable {
    
    // MARK: - Fields
    static let component_id: Int64 = 8
    static let table = Table("Takeables")
    static let takeable_id_column = Expression<Int64>("id")
    static let can_take_column = Expression<Bool>("can_take")
    static let message_column = Expression<String?>("message")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Takeable.takeable_id_column]
        }
    }
    
    var canTake: Bool {
        get {
            return row[Takeable.can_take_column]
        }
    }
    
    var message: String? {
        get {
            return row[Takeable.message_column]
        }
    }
    
    var entity: Entity? {
        get {
            return Entity.fetchByComponentID(Takeable.component_id, dataID: id)
        }
    }
}

extension Entity {
    func getTakeable() -> Takeable? {
        guard let component = getComponent(Takeable.component_id) else { return .none }
        let table = Takeable.table.filter(Takeable.takeable_id_column == component.dataID)
        guard let result = DataManager.instance.prepare(table).first else { return .none }
        return Takeable(row: result)
    }
}
