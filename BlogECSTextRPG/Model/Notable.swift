//
//  Notable.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Notable {
    
    // MARK: - Fields
    static let component_id: Int64 = 5
    static let table = Table("Notables")
    static let notable_id_column = Expression<Int64>("id")
    static let description_column = Expression<String?>("description")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Notable.notable_id_column]
        }
    }
    
    var description: String? {
        get {
            return row[Notable.description_column]
        }
    }
    
    var entity: Entity? {
        get {
            return Entity.fetchByComponentID(Notable.component_id, dataID: id)
        }
    }
}

extension Entity {
    func getNotable() -> Notable? {
        guard let component = getComponent(Notable.component_id) else { return .none }
        let table = Notable.table.filter(Notable.notable_id_column == component.dataID)
        guard let result = DataManager.instance.prepare(table).first else { return .none }
        return Notable(row: result)
    }
}
